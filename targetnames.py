from os import path
from metadata import MetaTags


class TargetNames(MetaTags):
    """Leverage metadata to generate target values. Useful when converting files
    to a new format."""

    def get_target_foldername(self) -> str:
        """Get the target folder name based on the object's metadata."""
        # Set the folder name: artist - album (year)
        return "{} - {} ({})".format(self.artist, self.album, self.date)

    def get_target_filename(self, target_extension="ogg") -> str:
        """Get the target filename based on object metadata."""
        # Set the file name: alphanumeric and underscores.
        return "{}_{}.{}".format(
            self.track_number.zfill(2),
            self.title.lower().replace(" ", "_"),
            target_extension,
        )

    def get_target_filepath(self) -> str:
        """Get the concatenated target file/folder name."""
        return path.join(self.get_target_foldername(), self.get_target_filename())
