"""
Handle metadata for a single track. Sanitize data from the file and get target
file/foldernames.
"""
from os import path
from re import match, sub
from audio_metadata import load


class MetaTags:
    """Store metadata and get target values (for example, the folder and
    filename).
    
    TODO: Change this name to AudioData or something..."""

    artist: str
    album: str
    date: str
    extension: str
    title: str
    track_number: int
    
    file_name: str      # Just the filename.
    file_path: str      # The absolute file path.
    folder_path: str    # The folder path (up to the filename).

    def __init__(self, folder_path: str, filename: str):
        """Constructor sets all fields.

        TODO: Have the source folder, filename, and extension be split from
        a *full file path* as the file name parameter."""
        
        self.file_name = filename
        self.extension = path.splitext(filename)[1]
        self.file_path = path.join(folder_path, filename)

        meta = load(self.file_path)["tags"]

        self.set_artist_name(meta["artist"][0])
        self.set_album_name(meta["album"][0])
        self.set_date(meta["date"][0])
        self.set_track_title(meta["title"][0])
        self.set_track_number(meta["tracknumber"][0])

    def set_artist_name(self, artist: str):
        self.artist = self._sanitize(artist)

    def set_album_name(self, album: str):
        self.album = self._sanitize(album)

    def set_date(self, date: str):
        self.date = self._sanitize(date)

    def set_track_title(self, title: str):
        self.title = self._sanitize(title)

    def set_track_number(self, track: int):
        """Set the track number. Use only numeric input, no letters (for example,
        A1 vs B1 for b-sides)."""
        self.track_number = match("(^[0-9]+)", track).groups()[0]

    def _sanitize(self, value: str) -> str:
        """Sanitize: alphanumeric and spaces."""
        return sub(r"[^0-9a-zA-Z\s]+", "", value)
