"""
Load configuration vaues from 
"""
import configparser
from pathlib import Path
from os import path, makedirs


# TODO: Let this be a value (comma separated) in the configuration file. The
# following method will use logic without setting anything in the global scope.
_VALID_EXT = (".flac", ".mp3", ".ogg", ".m4a", ".aac", ".alac", ".wav")


# TODO: Merge this logic with the config file later...
def is_valid_extension(file_name: str) -> bool:
    """Determine if a file extension is in the list of valid file extensions."""
    return path.splitext(file_name)[1] in _VALID_EXT


class Config:
    """Hold values from the configuration file in the user's home directory. Note
    that these values can be better stored in some kind of database that holds not
    only the configuration values, but also notes which tables (folders) are for
    `source` (libraries) and `output` (portable).

    TODO: Port these from classmethods to instance methods (`self`)."""

    @classmethod
    def __init__(cls, filename=None):

        # Initialize the parser.
        cls.conf = configparser.ConfigParser()

        # Default INI path, if none is given.
        if filename == None:
            inifolder = path.join(str(Path.home()), ".sinawi")
            cls.inifile = path.join(inifolder, "sinawi.ini")

        else:
            cls.inifile = filename

        try:
            makedirs(inifolder)

        except FileExistsError:
            print("{} already exists.".format(inifolder))

        cls.conf.read(cls.inifile)

    @classmethod
    def make_ini_template(cls) -> int:
        """Use this to avoid KeyErrors for initial set commands."""

        from os import path

        if path.exists(cls.inifile) == False:

            with open(cls.inifile, "w+") as ini:
                ini.write("[sinawi]")

            return True

        else:
            return False

    @classmethod
    def update_ini(cls):
        """Update currently loaded values."""

        with open(cls.inifile, "w+") as ini:
            cls.conf.write(ini)

        print("Values written to " + cls.inifile)

    @classmethod
    def read(cls):
        """Load values manually."""
        cls.conf.read(cls.inifile)

    @classmethod
    def get_music_dir(cls):
        """Get the music directory."""
        return cls.conf["sinawi"]["music_dir"]

    @classmethod
    def get_src_ext(cls):
        """Get the user's source directory."""
        return cls.conf["sinawi"]["src_extension"]

    @classmethod
    def get_out_ext(cls):
        """Get the user's output directory."""
        return cls.conf["sinawi"]["output_extension"]

    # Update the INI file's music directory.
    @classmethod
    def set_music_dir(cls, new_music_dir):

        cls.conf["sinawi"]["music_dir"] = new_music_dir
        print("New music directory: " + new_music_dir)
        cls.update_ini()

    @classmethod
    def set_src_ext(cls, new_src_ext):

        cls.conf["sinawi"]["src_extension"] = new_src_ext
        cls.update_ini()

    @classmethod
    def set_out_ext(cls, new_out_ext):

        cls.conf["sinawi"]["output_extension"] = new_out_ext
        cls.update_ini()
