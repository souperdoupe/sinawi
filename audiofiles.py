"""
Handle collections of audio file objects/data.
"""
from os import listdir
from configfile import is_valid_extension
from targetnames import TargetNames, MetaTags


class NoAudioFileException(Exception):
    def __init__(self, directory: str):
        """Raise when the dictionary does not populate. TODO: Move this to the converter..."""
        super().__init__(
            "Warning: No audio files were found in the given directory {}.".format(
                directory
            )
        )


class AudioFiles:
    """Composite class for all audio files in a given directory. Tradeoff:
    sacrificing some runtime (O(N^2)) for the sake of organizing, especially for a
    GUI implementation.

    The `audio_files` collection holds audio file data in the form:
    `< file path : Metadata object >`. This should make it easier to parse the
    full file paths during conversion. Also, this should make it easier to compare
    (later) if a file exists in the output directory (to prevent duplicate
    conversions)."""

    audio_files: dict
    output_folder: str

    def __init__(self, source_folder: str):
        """Constructor initializes the internal data types. If a root folder
        and target extension is set, also loads all valid audio files from that
        folder."""

        self.audio_files = {}

        if source_folder:
            self.load_audio_files(source_folder)

        if not len(self.audio_files):
            raise NoAudioFileException(source_folder)

    def load_audio_files(self, source_folder: str):
        """Load the collection of audio files from a given folder. The target
        extension can be any valid file extension (default: vorbis)."""

        for filename in listdir(source_folder):

            if is_valid_extension(filename):
                self.add_audio_file(source_folder, filename)

    def add_audio_file(self, folder: str, filename: str):
        """Add a single audio and its directory path to the collection."""

        # Create an object to hold metadata and target path names.
        file_data = MetaTags(folder, filename)

        # Add the audio file object to the dictionary.
        self.audio_files[file_data.file_path] = file_data

    def get_file_paths(self) -> list:
        """Return the sorted list of all audio files (full paths)."""
        return sorted(self.audio_files.keys())

    def get_file_data(self, full_pathname: str) -> MetaTags:
        """Get data for a single audio object based on the full pathname."""
        return self.audio_files[full_pathname]

    def get_file_objects(self) -> list:
        """Get a list of all file objects."""
        return self.audio_files.values()

    def get_target_directories(self) -> set:
        """Get a list of unique folder names from the audio file collection."""
        return set([v.get_target_foldername() for v in self.audio_files.values()])
    

class TargetAudioFiles(AudioFiles):
    """Store a collection of TargetNames types. Leverage the TargetNames methods 
    (for example, during conversion)."""

    def add_audio_file(self, folder: str, filename: str):
        """Add a single audio and its directory path to the collection."""

        # Create an object to hold metadata and target path names.
        file_data = TargetNames(folder, filename)

        # Add the audio file object to the dictionary.
        self.audio_files[file_data.get_target_filepath()] = file_data