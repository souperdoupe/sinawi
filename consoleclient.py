from argparse import ArgumentParser
from converter import Converter
from audiofiles import NoAudioFileException
from configfile import Config
from database import DbTool
from os import path


class ConsoleArgs:
    """Handle all command-line arguments. TODO: Figure out why dashes won't work
    when fetching the values from the `arg` object..."""

    args = None  # Data type is whatever parse_args returns...

    def __init__(self):
        """Handle all command-line arguments. Store the user-given values in the
        `args` field."""

        parser = ArgumentParser()

        parser.add_argument(
            "--convert",
            type=str,
            help="Manually convert files in the given path (relative/absolute).",
        )

        parser.add_argument(
            "--makedatabase", action="store_true", help="Create the initial database."
        )

        parser.add_argument(
            "--set-converted-dir",
            type=str,
            help="Set the target directory for converted files.",
        )

        parser.add_argument(
            "--set-output-ext",
            type=str,
            help="Set the output directory in the config file.",
        )

        parser.add_argument(
            "--set-source-ext", type=str, help="Specify the current source directory."
        )

        parser.add_argument(
            "--maketemplate",
            action="store_true",
            help="Make the initial template for the INI config file.",
        )

        # Set the instance field with the current arguments/values.
        self.args = parser.parse_args()


class ConsoleClient(ConsoleArgs):
    """Leverage the console argument class to automate actions."""

    def __init__(self):
        """Perform actions based on user-specified arguments."""

        super().__init__()

        if self.args.makedatabase:
            d = DbTool()
            d.make_library_table()
            d.update_library_db()

        if self.args.setconverteddir:
            Config().set_music_dir(self.args.setlibrarydir)

        if self.args.setoutputdir:
            Config().set_out_ext(self.args.setoutputdir)

        if self.args.setsourcedir:
            Config().set_src_ext(self.args.setsourcedir)

        if self.args.maketemplate:
            Config().make_ini_template()

        if self.args.convert:

            try:
                s = Converter(path.abspath(self.args.convert))
                s.convert_all_files()

            except NoAudioFileException as e:
                print(e)
