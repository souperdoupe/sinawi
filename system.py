"""
Perform system commands (from the shell).
"""
from shlex import quote
from os import system


class SystemCommand:
    """Abstract class/template to illustrate how each driver should look."""
    
    output_extension: str
    options: str

    def __init__(self, output_extension=".ogg", options="-qscale:a 7 -vn"):
        """Constructor should set options that are specific to whatever coded or
        tool is used to perform the conversion."""

        self.output_extension = output_extension
        self.options = options
    
    def is_installed(self) -> bool:
        """Check if the system utility is installed."""
        return None

    def run(self, infile: str, outfile: str):
        """Run any codec driver to convert a single audio file. Both arguments
        should be full, valid path names."""
        return None


class SingleFileDriver(SystemCommand):
    """Leverage ffmpeg to convert an audio file.

    TODO: Write logic to determine if ffmpeg is installed on the current system."""

    def run(self, infile: str, outfile: str, cue_file=None):
        """Use ffmpeg to convert just one song.
        TODO: Incorporate shntool to split the flac file, then convert..."""

        system(f"ffmpeg -i {quote(infile)} {self.options} {quote(outfile)}")


class CuefileDriver(SystemCommand):
    """Convert songs from a cuefile, one by one.
    
    TODO: Review the Arch entry... test the shell command elsewhere.
        https://wiki.archlinux.org/title/CUE_Splitting
        https://linux.die.net/man/1/oggenc
    """
    
    def run(self, cue_file, outfile, flac_file):
        "shnsplit -f {} -o {} {}".format(
            quote(cue_file),
            quote(f"cust ext=ogg oggenc {self.options} -o {quote(outfile)} -"),
            quote(flac_file)
        )