#!/bin/bash

# Note: If this fails, remove the Windows line endings
#       or run dos2unix on this script.

OUTDIR="/opt/sinawi"
APP_MODULE="sinawi"
BINDIR="/usr/local/bin"

# Make the directory and all subdirectories, if needed.
mkdir -p $OUTDIR

# Copy and remove windows line endings.
for i in *.py; do 

    cp "$i" $OUTDIR && \
    dos2unix $OUTDIR"/$i"

done && \

# Make a symlink to the application.
ln -s $OUTDIR/$APP_MODULE".py" $BINDIR/$APP_MODULE && \
chmod +x $OUTDIR/$APP_MODULE".py"

# Print a simple status message.
echo "Done." || echo "Errors occurred."
