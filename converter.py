"""
Convert an entire directory of music.
"""
from os import path, makedirs
from configfile import Config
from audiofiles import TargetAudioFiles
from system import SystemCommand, SingleFileDriver, CuefileDriver


class Converter:
    """Convert all audio files from a target folder."""

    source_ext: str
    output_ext: str
    source_directory: str
    output_library: str

    audio_files: TargetAudioFiles  # Collection of audio files to be converted.
    driver: SystemCommand

    def __init__(self, source_directory: str, cue_file=False):
        """The constructor sets all configuration variables and loads the objects
        from the target directory. Source directory should include the *full path*
        to the folder you want to convert."""

        self._set_from_config()
        self.audio_files = TargetAudioFiles(source_directory)
        self.driver = CuefileDriver if cue_file else SingleFileDriver  # TODO: TEST!!!

    def convert_all_files(self):
        """Convert all audio files in the target collection."""

        self.make_all_folders()

        for file in self.audio_files.get_file_objects():                    
            SingleFileDriver().run(
                file.file_path, 
                path.join(self.output_library, file.get_target_filepath())
            )

    def make_all_folders(self):
        """Generate all folders in the colelction of target audio files."""

        for folder in self.audio_files.get_target_directories():
            self.make_folder(path.join(self.output_library, folder))

    def make_folder(self, folder_path: str):
        """Make the path if it doesn't exist."""

        if not path.exists(folder_path):
            makedirs(folder_path)

    def _set_from_config(self):
        """Update all variables from the user's configuration file."""

        conf = Config()

        # Note: Assume a KeyError (key:value) is related to a bad/missing INI.
        self.source_ext = conf.get_src_ext()
        self.output_ext = conf.get_out_ext()
        self.output_library = conf.get_music_dir()
