from shlex import quote
from os import system
from converter import Converter


class ConvertFromCuefile(Converter):
    """Needed for files like FLAC and WAV."""
    
    
    def split_from_cuefile(self, cue_file: str, flac_file: str):
        system(
            f"shnsplit -f {quote(cue_file)} -t %n-%t -o flac {quote(flac_file)}"
        )
        
