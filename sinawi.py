#!/usr/bin/env python3
"""
Convert directories of flac files to vorbis.
    
This should work for pretty much any standard file format. The big exception 
right now is WAV/CUE.
"""
from consoleclient import ConsoleClient


if __name__ == "__main__":
    ConsoleClient()
