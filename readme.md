# Sinawi

Simple tool for managing your music.

# Purpose

Sinawi converts your lossless albums into portable (lossy) files. In its
current form, Sinawi converts FLAC input to Vorbis (.ogg). Unlike mp3’s,
Vorbis is a fully open-source format. It has been proven to produce
better audio quality and compresses better than mp3’s. The only downside
is that many audio players do not recognize .ogg files.

Meta tags are handled on a per-file basis. Data is used to format the
output folder and filename pattern.

This script is meant to be modified based on your needs. Just modify the
script if you want to convert to or from anything other than FLAC or
Vorbis.

# Installation

You should have some idea of where you want your “media directory”
located. Edit the `outdir` variable in the script to match that output
directory. Then, run:

    chmod +x install.sh && ./install.sh

# Configuration

After installation, you can run any of the following commands to set up
your config (`~/.sinawi.ini`):

    # Choose the directory where your converted music goes.
    sinawi --set-dir ~/music
    
    # Choose a lossless extension, like FLAC.
    sinawi --set-ext-src .flac
    
    # Choose a lossy extension, like OGG Vorbis.
    sinawi --set-ext-out .ogg

If `~/.sinawi.ini` does not exist, Sinawi will automatically set up a
template for you. When it does this, make sure to re-run the command.

Once all three commands are executed successfully, a complete config
file will look like:

``` ini
[sinawi]
; Where your converted music will go.
music_dir = /your/music/dir

; Your lossless source.
src_extension = .flac

; Your lossy converted format.
output_extension = .ogg
```

# Usage

## From the command line

By default, sinawi converts files in the current working directory. This
is likely to change in the future.

To use it, enter the directory of your source album (eg, the FLAC
version). Then, run `sinawi`. The program will take care of the rest.

## Other ideas

Sinawi can also be used as a “context menu” (right-click) option. Many
popular file managers, like Thunar or PCManFM/-qt, allow such
functionality. In this way, your normal file manager will also become
your media manager.

# Defaults

| Data              | Format                           |
| :---------------- | :------------------------------- |
| Input file        | FLAC (.flac)                     |
| Output file       | Vorbis (.ogg)                    |
| Output foldername | Artist - Album (Date)            |
| Output filename   | traknumber-track\_name.extension |

# Caveats

This program is a simple converter. With that in mind, consider the
following caveats for its usage.

## Syncing across devices

You’re on your own with this one. Consider the following alternatives as
starting points:

  - USB Mass Storage

  - MTP (Android’s default)

  - sftp/ssh internal-sftp server (even just a local one)

Here’s an example solution:

1.  Find a way to mount your device.
    
      - Wired: Android could use MTP through USB.
    
      - Wireless: You could run sshd though an application like termux.
        Then, mount it on your host system with sshfs.

2.  Set Sinawi’s outdir variable to match your device’s mountpoint.

3.  Convert your files on a per-file basis.

## Database of albums-to-be-converted

Sinawi simply doesn’t implement this. Sounds like a great idea for
another project.

Example solution:

1.  Make a list with the full pathnames of your source albums.

2.  Implement function that loops through each line, and passes it to
    Sinawi as the argument.

3.  Refer to the previous section for ideas on how to handle file
    transfer.
