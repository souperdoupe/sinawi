"""
Leverage a database and corresponding module to store and use information about 
libraries and output directories. Note that later versions of this code should
generalize the database superclass from implementations. (For example, this
version uses sqlite3. Later versions could use mongo, firebase, etc.)
"""
from sqlite3 import connect
from os import path
import os
from pathlib import Path


class DbTool:
    """Manage the database, which should contain tables that represent the
    library/libraries and output directory/directories.

    TODO: Rename this class (ex, `Database`).

    TODO: Consider reworking this for *just* generic database calls. Rework the
    language to fit any table type (not specific ones that refer to a specific
    table)."""

    def __init__(
        self,
        music_path=None,
        db_path=None,
        db_name="sinawi",
        library="music_src",
        portable="portable",
    ):
        """Constructor sets all values needed to establish a connection. It also
        sets values that refer to specific tables. Future versions of this app
        should generalize the definitions here and allow the operator to refer to
        any table in the database, not just hardcoded ones."""

        # Default to the user's home "Music" folder.
        if not music_path:
            music_path = os.path.join(Path.home(), "Music")

        # Default database directory in home.
        if not db_path:
            db_path = os.path.join(Path.home(), "sinawi.db")

        self.MUSIC_PATH = music_path
        self.DB_PATH = db_path  # Note: not yet sure how to specify path.
        self.DB_NAME = db_name
        self.LIBRARY = library
        self.PORTABLE = portable

        # Connect to the database.
        db = connect(db_path)

        # Make pointers to some common database methods.
        self.cursor = db.cursor()
        self.execute = db.execute
        self.commit = db.commit

    def make_library_table(self):
        """Create the library from scratch."""

        # Note: Don't need to run a `create database` command.
        self.execute(
            """
                CREATE TABLE 
                {} (filepath string, filename string, suffix string) 
            """.format(
                self.LIBRARY,
            )
        )

        self.commit()

    def update_library_db(self):
        """Scan the library and update entries."""

        for root, _, files in os.walk(self.MUSIC_PATH):

            for file in files:

                fpath = os.path.join(root, file)
                file_name, file_ext = os.path.splitext(file)

                self.add_to_library(fpath, file_name, file_ext)

    def add_to_library(self, filepath: str, filename: str, suffix: str):
        """Add a single file/path to the library."""

        self.execute(
            """
                    INSERT INTO {} (filepath, filename, suffix) VALUES (?, ?, ?)
                    """.format(
                self.LIBRARY
            ),
            (
                filepath,
                filename,
                suffix,
            ),
        )

        self.commit()

    def query_library_filepaths(self, search_string):
        """Get the file paths from the current library (database)."""

        self.cursor(
            """
                SELECT filepath FROM {} WHERE filepath LIKE ?;
                """.format(
                self.LIBRARY
            ),
            (search_string,),
        )

        return self.cursor.fetchall()[0]

    def update_portable_db(self):
        """Scan/update the folder of portable files. TODO: Implement..."""
        return

    def add_to_portable(self, filepath: str):
        """Insert a single entry to the portable database."""

        filename = path.split(filepath)

        self.execute(
            """INSERT INTO {} (filepath, filename) 
                            VALUES(?, ?)""".format(
                self.PORTABLE
            ),
            (
                filepath,
                filename,
            ),
        )

    def query_portable(self, column="*", where_fpath=None) -> tuple:
        """Get the query data as a tuple."""

        query = """SELECT {} FROM {};
                    """.format(
            column, self.PORTABLE
        )

        if where_fpath is not None:
            query = query.replace(";", "WHERE filepath = ?;")

        # NOTE: THIS COULD BREAK. ARGUMENT WITHOUT ? IN QUERY.
        self.cursor(query, (where_fpath,))

        return self.cursor.fetchall()

    def del_from_portable(self):
        """Deletes an entry from the portable folder. TODO: Implement..."""
        return
